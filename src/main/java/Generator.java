
import importer.Importer;
import importer.ImporterTsv;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import entity.Config;
import entity.InnerDataFormat;
import exporter.Exporter;
import exporter.ExporterPages;
import exporter.XMLConfigReaderJaxb;

public class Generator {

	public static void main(String[] args) throws IOException, XMLStreamException, JAXBException {

		String fileConfigExporter = args[0];
		String fileSourceData = args[1];
		String fileReport = args[2];

		Importer importer = new ImporterTsv();
		importer.importData(new FileInputStream(fileSourceData));
		InnerDataFormat inDataFmt = importer.getData();

		Exporter exporter = new ExporterPages();
//		Config config = new XMLConfigReader().read(new FileInputStream(fileConfigExporter));
		Config config = new XMLConfigReaderJaxb().read(new FileInputStream(fileConfigExporter));
		exporter.importData(config, inDataFmt);
		exporter.exportData(new FileOutputStream(fileReport));
		System.out.println("Work done.");
	}

}
