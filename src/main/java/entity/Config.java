package entity;


import java.io.InputStream;

import javax.xml.bind.JAXBException;

public interface Config {

	PageFmt getPageFormat();

	Config read(InputStream in) throws JAXBException;

}
