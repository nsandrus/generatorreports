package entity;


import java.io.InputStream;

public interface ConfigReader {

	Config read(InputStream in);

}
