package entity;

import java.util.List;

public interface InnerDataFormat {

	static final String INNER_DELIMETER = "\t";

	List<LineData> getData();

	String getInnerDelimeter();

}
