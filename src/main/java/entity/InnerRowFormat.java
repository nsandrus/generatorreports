package entity;

import java.util.ArrayList;
import java.util.List;

public class InnerRowFormat implements InnerDataFormat {

	private List<LineData> row = new ArrayList<>();

	public InnerRowFormat(List<LineData> rows) {
		this.row = rows;
	}

	public String getInnerDelimeter() {
		return INNER_DELIMETER;
	}

	@Override
	public List<LineData> getData() {
		return row;
	}

}
