package entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LineData {

	private List<String> line = new ArrayList<>();

	public LineData(String[] split) {
		line = Arrays.asList(split);
	}

	public LineData(final List<String> titleRow) {
		line = titleRow;
	}

	public List<String> getLine() {
		return line;
	}

	
}
