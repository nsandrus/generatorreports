package entity;

import java.util.List;

import exporter.entity.Column;
import exporter.entity.Settings;

public class PageFmt {

	private Settings settings;

	public PageFmt(Settings settings) {
		this.settings = settings;
	}

	public PageFmt() {
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	@Override
	public String toString() {
		return "PageFmt [settings=" + settings + "]";
	}

	public List<Column> getColumn() {
		return settings.getColumns().getColumn();
	}

	public int getHeight() {
		return settings.getPage().getHeight();
	}

	public int getWidth() {
		return settings.getPage().getWidth();
	}

}
