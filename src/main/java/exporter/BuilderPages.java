package exporter;

import java.util.ArrayList;
import java.util.List;

import entity.LineData;
import entity.PageFmt;
import exporter.entity.Column;
import exporter.entity.PageTable;
import exporter.entity.RowTable;

public class BuilderPages {

	private static final int HEIGHT_ROW_DELIMETER = 1;
	private static final String SIMBOL_DELIMETER_ROWS = "-";
	private static String lineSeparator = System.getProperty("line.separator");

	private List<Integer> listWidthColumns;
	private List<LineData> data;
	private String endPageChar;
	private PageFmt pageFmt;
	private List<String> titleRow = new ArrayList<>();

	public BuilderPages(List<LineData> data, PageFmt pageFmt) {
		this.data = data;
		this.pageFmt = pageFmt;
		endPageChar = "~";
		setTitleRow();
		setListWidthColumns();
	}

	private void setTitleRow() {
		for (Column col : pageFmt.getColumn()) {
			titleRow.add(col.getTitle());
		}
	}

	public List<PageTable> build() {
		List<PageTable> pages = new ArrayList<>();
		PageTable page = new PageTable();

		RowTable titleRow = new RowTable(getTitleText(), listWidthColumns);
		int hRowTitle = titleRow.getHeight();
		int pageHeight = hRowTitle;
		String exportTitleText = RowTableBuilder.build(titleRow, lineSeparator);
		page.setTitle(exportTitleText);

		for (LineData line : data) {
			RowTable rowTable = new RowTable(line, listWidthColumns);
			pageHeight += rowTable.getHeight() + HEIGHT_ROW_DELIMETER;

			if (pageHeight > pageFmt.getHeight()) {
				pageHeight = hRowTitle;
				page.addRowTable(endPageChar + lineSeparator);
				pages.add(page);
				page = new PageTable();
				page.setTitle(exportTitleText);
			}

			page.addRowTable(getLineDelimeterRows());
			page.addRowTable(RowTableBuilder.build(rowTable, lineSeparator));
		}
		pages.add(page);
		return pages;
	}

	private LineData getTitleText() {
		LineData colNames = new LineData(titleRow);
		return colNames;
	}

	private String getLineDelimeterRows() {
		int width = pageFmt.getWidth();
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < width; i++) {
			str.append(SIMBOL_DELIMETER_ROWS);
		}
		str.append(lineSeparator);
		return str.toString();
	}

	private void setListWidthColumns() {
		listWidthColumns = new ArrayList<>();
		for (Column col : pageFmt.getColumn()) {
			listWidthColumns.add(new Integer(col.getWidth()));
		}
	}
}
