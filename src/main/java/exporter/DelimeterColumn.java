package exporter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import exporter.entity.CellTable;

public class DelimeterColumn {

	public static CellTable getCellTable(String colText, int width) {
		CellTable cell = new CellTable();
		StringBuilder text = new StringBuilder(colText);
		int pos;

		while (text.length() > width) {
			text = deleteWhiteSpaceAtFirst(text);
			pos = getPositionInText(text, width);
			pos = posLessWidth(width, text, pos);
			pos = getBoundedPosition(width, pos);
			cell.addLine(text.substring(0, pos));
			text.delete(0, pos);
		}

		text = deleteWhiteSpaceAtFirst(text);
		cell.addLine(text.toString());
		return cell;
	}

	private static int posLessWidth(int width, StringBuilder text, int pos) {
		if (pos < width) {
			pos++; // если переносимый символ уместиться на строке, оставить его тут
			pos = getPosFutureWord(width, text, pos);
		}
		return pos;
	}

	private static int getBoundedPosition(int width, int pos) {
		// нет разделителя или на границе, резать по правой границе, в ТЗ посередине
		if (pos == 0 || pos == -1) {
			pos = width;
		}
		return pos;
	}

	private static int getPosFutureWord(int width, StringBuilder text, int posDelim) {
		int right = getNextDelimeter(text, width);
		if (right > posDelim + width) {
			// System.out.println("не надо переностить, режем как без переноса");
			posDelim = width;
		}
		return posDelim;
	}

	private static StringBuilder deleteWhiteSpaceAtFirst(StringBuilder text) {
		if (text.charAt(0) == ' ') {
			text.deleteCharAt(0);
		}
		return text;
	}

	private static int getPositionInText(StringBuilder str, int pos) {
		int i = pos;
		for (; i >= 0; i--) {
			if (isDelimeter(str.charAt(i))) {
				return i;
			}
		}
		return i;
	}

	private static int getNextDelimeter(StringBuilder str, int pos) {
		int i = pos;
		for (; i < str.length(); i++) {
			if (isDelimeter(str.charAt(i))) {
				return i;
			}
		}
		return i;
	}

	private static boolean isDelimeter(char c) {
		String simbol = String.valueOf(c);
		Pattern p = Pattern.compile("[a-z0-9а-яА-Я]");
		Matcher m = p.matcher(simbol);
		return !m.matches();
	}

}
