package exporter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import entity.Config;
import entity.InnerDataFormat;

public interface Exporter {

	void importData(Config cfg, InnerDataFormat idf);

	void exportData(OutputStream out) throws UnsupportedEncodingException, IOException;

}
