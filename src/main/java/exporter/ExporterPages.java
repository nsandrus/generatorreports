package exporter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;

import entity.Config;
import entity.InnerDataFormat;
import entity.LineData;
import entity.PageFmt;
import exporter.entity.PageTable;

public class ExporterPages implements Exporter {

	private PageFmt pageFmt;
	private List<LineData> data;

	@Override
	public void importData(Config cfg, InnerDataFormat idf) {
		this.data = idf.getData();
		pageFmt = cfg.getPageFormat();
	}

	@Override
	public void exportData(OutputStream os) throws IOException {
		BuilderPages pages = new BuilderPages(data, pageFmt);
		exportPages(os, pages.build());
	}

	private void exportPages(OutputStream os, List<PageTable> pages) throws UnsupportedEncodingException, IOException {
		Writer out = new BufferedWriter(new OutputStreamWriter(os, "UTF-16"));
		for (PageTable pagetmp : pages) {
			out.append(pagetmp.getTitle());
			for (String line : pagetmp.getRows()) {
				out.append(line);
			}
		}
		out.flush();
		out.close();
	}

}
