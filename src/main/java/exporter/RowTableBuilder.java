package exporter;

import java.util.Formatter;
import java.util.List;

import exporter.entity.CellTable;
import exporter.entity.RowTable;

public class RowTableBuilder {

	public static String build(RowTable rowTable, String lineSeparator) {
		int height = rowTable.getHeight();
		List<Integer> sizeColumns = rowTable.getWidthColumnsList();
		StringBuilder sb = new StringBuilder();
		Formatter formatter = new Formatter(sb);
		String colText;
		for (int idxLine = 0; idxLine < height; idxLine++) {
			formatter.format("|");
			int cellIndex = 0;
			for (CellTable cell : rowTable.getRowTable()) {
				colText = "";
				if (idxLine < cell.height()) {
					colText = cell.getLine(idxLine);
				} else {
					colText = "";
				}
				formatter.format(" %1$-" + sizeColumns.get(cellIndex) + "s |", colText);
				cellIndex++;
			}

			formatter.format(lineSeparator);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
