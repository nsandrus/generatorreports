package exporter;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import entity.Config;
import entity.PageFmt;
import exporter.entity.Settings;

public class XMLConfigReaderJaxb implements Config {

	private PageFmt pageFmt;
	
	@Override
	public PageFmt getPageFormat() {
		return pageFmt;
	}

	@Override
	public Config read(InputStream in) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Settings.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		Settings settings = (Settings) unmarshaller.unmarshal(in);
		pageFmt = new PageFmt(settings);
		return this;
	}

}
