package exporter.entity;

import java.util.ArrayList;
import java.util.List;

public class CellTable {

	private List<String> cell = new ArrayList<>();

	public List<String> getCell() {
		return cell;
	}

	public void setCell(List<String> cell) {
		this.cell = cell;
	}

	public void addLine(String line) {
		cell.add(line);

	}

	public int height() {
		return cell.size();
	}

	public String getLine(int line) {
		return cell.get(line);
	}
}
