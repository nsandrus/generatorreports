package exporter.entity;

import java.util.ArrayList;
import java.util.List;

public class PageTable {

	private String title;
	private List<String> rows = new ArrayList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getRows() {
		return rows;
	}

	public void setRows(List<String> rows) {
		this.rows = rows;
	}

	public void addRowTable(String printLineRow) {
		rows.add(printLineRow);

	}

	
	
}
