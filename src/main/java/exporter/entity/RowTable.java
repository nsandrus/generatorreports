package exporter.entity;

import java.util.ArrayList;
import java.util.List;

import entity.LineData;
import exporter.DelimeterColumn;

public class RowTable {

	private int maxHeight;
	private List<Integer> widthColumnsList;
	private List<CellTable> rowTable = new ArrayList<>();;

	public RowTable(LineData lineData, List<Integer> listWidthColumns) {
		this.widthColumnsList = listWidthColumns;
		List<String> fields = lineData.getLine();
		for (int i = 0; i < fields.size(); i++) {
			int widthColumn = widthColumnsList.get(i);
			CellTable cell = getLines(fields.get(i), widthColumn);
			updateMaxHeight(cell);
			rowTable.add(cell);
		}

	}

	private void updateMaxHeight(CellTable cell) {
		if (cell.height() > maxHeight) {
			maxHeight = cell.height();
		}
	}

	public List<CellTable> getRowTable() {
		return rowTable;
	}

	public void setRowTable(List<CellTable> rowTable) {
		this.rowTable = rowTable;
	}

	private CellTable getLines(String colText, int width) {
		CellTable cell = new CellTable();
		if (colText.length() <= width) {
			cell.addLine(colText);
			return cell;
		}
		return DelimeterColumn.getCellTable(colText, width);
	}

	public Integer getHeight() {
		return maxHeight;
	}

	public List<Integer> getWidthColumnsList() {
		return widthColumnsList;
	}

}
