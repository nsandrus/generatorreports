package importer;

import java.io.IOException;
import java.io.InputStream;

import entity.InnerDataFormat;

public interface Importer {

	void importData(InputStream in) throws IOException;

	InnerDataFormat getData();

}
