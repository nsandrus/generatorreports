package importer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import entity.InnerDataFormat;
import entity.InnerRowFormat;
import entity.LineData;

public class ImporterTsv implements Importer {

	private static final String DELIMETER_TSV = "\t";
	private List<LineData> rows = new ArrayList<>();

	@Override
	public void importData(InputStream in) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-16"));
		String line;
		while ((line = reader.readLine()) != null) {
			rows.add(new LineData(line.split(DELIMETER_TSV)));
		}
		reader.close();
	}

	@Override
	public String toString() {
		return "ImporterTsv [rows=" + rows + "]";
	}

	@Override
	public InnerDataFormat getData() {
		InnerDataFormat data = new InnerRowFormat(rows);
		return data;
	}

}
