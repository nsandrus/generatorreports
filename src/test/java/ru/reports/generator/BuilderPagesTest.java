package ru.reports.generator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import entity.LineData;
import entity.PageFmt;
import exporter.BuilderPages;
import exporter.entity.Column;
import exporter.entity.Columns;
import exporter.entity.Page;
import exporter.entity.PageTable;
import exporter.entity.Settings;

public class BuilderPagesTest extends TestCase {

	PageFmt pageFmt = new PageFmt();
	Map<String, Integer> columnsSize;
	List<LineData> data;
	Settings set;

	protected void setUp() {
		columnsSize = new LinkedHashMap<>();
		data = new ArrayList<LineData>();
		set = new Settings();
		Page page = new Page();
		page.setWidth(12);
		page.setHeight(10);
		set.setPage(page);
		Columns columns = new Columns();
		List<Column> columnList = new ArrayList<>();
		columnList.add(new Column("Номер", 4));
		columnList.add(new Column("Дата", 4));
		columnList.add(new Column("ФИО", 4));
		columns.setColumn(columnList);
		set.setColumns(columns);
		pageFmt.setSettings(set);
	}

	public void testNormalPages() {
		String[] lines = { "1", "12-12-2017", "Иван" };
		data.add(new LineData(lines));

		List<PageTable> pages = new BuilderPages(data, pageFmt).build();
		assertEquals("| Номе | Дата | ФИО  |\r\n| р    |      |      |\r\n", pages.get(0).getTitle());
		assertEquals("------------\r\n", pages.get(0).getRows().get(0));
		assertEquals("| 1    | 12-  | Иван |\r\n|      | 12-  |      |\r\n|      | 2017 |      |\r\n", pages.get(0).getRows().get(1));
	}

	public void testNormalPages2() {
		set.getColumns().getColumn().get(2).setWidth(6);
		set.getPage().setHeight(5);

		String[] lines = { "1", "12-12-2017", "Иванов Иван Иванович" };
		data.add(new LineData(lines));

		List<PageTable> pages = new BuilderPages(data, pageFmt).build();
		assertEquals("| Номе | Дата | ФИО    |\r\n| р    |      |        |\r\n", pages.get(0).getTitle());
		assertEquals("~\r\n", pages.get(0).getRows().get(0));
		assertEquals("| Номе | Дата | ФИО    |\r\n| р    |      |        |\r\n", pages.get(1).getTitle());
		assertEquals("------------\r\n", pages.get(1).getRows().get(0));
		assertEquals("| 1    | 12-  | Иванов |\r\n|      | 12-  | Иван И |\r\n|      | 2017 | ванови |\r\n|      |      | ч      |\r\n", pages.get(1).getRows().get(1));

	}

}
