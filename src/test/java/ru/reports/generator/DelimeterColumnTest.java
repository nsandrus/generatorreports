package ru.reports.generator;

import exporter.DelimeterColumn;
import exporter.entity.CellTable;
import junit.framework.TestCase;

public class DelimeterColumnTest extends TestCase {

	public void testTire() {
		String text = "Орехово-Зуево";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Орехово", cell.getLine(0));
		assertEquals("-Зуево", cell.getLine(1));
	}

	public void testWhiteSpace() {
		String text = "Орехово Зуево";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Орехово", cell.getLine(0));
		assertEquals("Зуево", cell.getLine(1));
	}

	public void testWhite3() {
		String text = "Орехово Зуево";
		CellTable cell = DelimeterColumn.getCellTable(text, 3);
		assertEquals("Оре", cell.getLine(0));
		assertEquals("хов", cell.getLine(1));
		assertEquals("о З", cell.getLine(2));
		assertEquals("уев", cell.getLine(3));
		assertEquals("о", cell.getLine(4));
	}

	public void testWhite32() {
		String text = "Орехово З уево";
		CellTable cell = DelimeterColumn.getCellTable(text, 3);
		assertEquals("Оре", cell.getLine(0));
		assertEquals("хов", cell.getLine(1));
		assertEquals("о З", cell.getLine(2));
		assertEquals("уев", cell.getLine(3));
		assertEquals("о", cell.getLine(4));
	}

	public void testWhite33() {
		String text = "Ким Чен Ир";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Ким Чен", cell.getLine(0));
		assertEquals("Ир", cell.getLine(1));
	}

	public void testWhite34() {
		String text = "Ким Ченыр";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Ким ", cell.getLine(0));
		assertEquals("Ченыр", cell.getLine(1));
	}

	public void testWhite35() {
		String text = "Ким Ченыршов";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Ким Чен", cell.getLine(0));
		assertEquals("ыршов", cell.getLine(1));
	}

	public void testWhite3tire() {
		String text = "Орехово-Зуево";
		CellTable cell = DelimeterColumn.getCellTable(text, 3);
		assertEquals("Оре", cell.getLine(0));
		assertEquals("хов", cell.getLine(1));
		assertEquals("о-З", cell.getLine(2));
		assertEquals("уев", cell.getLine(3));
		assertEquals("о", cell.getLine(4));
	}

	public void testDash() {
		String text = "Орехово&Зуево#";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Орехово", cell.getLine(0));
		assertEquals("&Зуево#", cell.getLine(1));
	}

	public void test4Line() {
		String text = "Орехово&Зуево2AND3LINE";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Орехово", cell.getLine(0));
		assertEquals("&Зуево2", cell.getLine(1));
		assertEquals("AND3LIN", cell.getLine(2));
		assertEquals("E", cell.getLine(3));
	}

	public void testBigLine() {
		String text = "Орехово2Зуево";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Орехово", cell.getLine(0));
		assertEquals("2Зуево", cell.getLine(1));
	}

	public void testSmallLines() {
		String text = "Иванов Иван Степан2";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("Иванов ", cell.getLine(0));
		assertEquals("Иван ", cell.getLine(1));
		assertEquals("Степан2", cell.getLine(2));
	}

	public void testSimbols() {
		String text = "!~@#$%^&*()_=+}{][/'\"";
		CellTable cell = DelimeterColumn.getCellTable(text, 7);
		assertEquals("!~@#$%^", cell.getLine(0));
		assertEquals("&*()_=+", cell.getLine(1));
		assertEquals("}{][/'\"", cell.getLine(2));
	}

}
