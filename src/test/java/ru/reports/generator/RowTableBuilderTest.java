package ru.reports.generator;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;
import entity.LineData;
import exporter.RowTableBuilder;
import exporter.entity.RowTable;

public class RowTableBuilderTest extends TestCase {

	String lineSeparator = "\n";

	public void test1() {
		List<Integer> listWidthColumns = Arrays.asList(2, 3, 4);
		String[] lineWithDelim = { "1", "what", "big deal" };
		LineData lineData = new LineData(lineWithDelim);

		RowTable rowTable = new RowTable(lineData, listWidthColumns);
		String formatRow = RowTableBuilder.build(rowTable, lineSeparator);
		assertEquals("| 1  | wha | big  |\n|    | t   | deal |\n", formatRow);
	}

	public void test3() {
		List<Integer> listWidthColumns = Arrays.asList(2, 3, 4);
		String[] lineWithDelim = { "12345678", "what", "big deal" };
		LineData lineData = new LineData(lineWithDelim);
		RowTable rowTable = new RowTable(lineData, listWidthColumns);
		String formatRow = RowTableBuilder.build(rowTable, lineSeparator);
		assertEquals("| 12 | wha | big  |\n| 34 | t   | deal |\n| 56 |     |      |\n| 78 |     |      |\n", formatRow);

	}

}
